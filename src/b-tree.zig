const std = @import("std");

pub fn main() !void {
    const order = 3;
    comptime var keys = [_]u8{ 1, 2, 3 };
    comptime var myBTree = BTree(u8, order){};
    const myBTreeinit = comptime try myBTree.init(&keys);
    const k = std.fmt.comptimePrint("This can't be empty: {any}", .{myBTreeinit.type});
    std.debug.print("{s}\n", .{k});
}

pub fn BTree(comptime T: type, comptime order: usize) type {
    return struct {
        const BTree_Node = union(enum) {
            Leaf_Node: Leaf_Node,
            Inner_Node: Inner_Node,
        };

    };
}
